﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName ="Float Value")]
public class FloatValue : ScriptableObject
{
    public float Value;
}
