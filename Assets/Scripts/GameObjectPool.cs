﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectPool : MonoBehaviour
{
    [SerializeField] private GameObject prifab;
    [SerializeField] private int numberOfObjects;
    private List<GameObject> gameObjectsPool = new List<GameObject>();
    public Action GetGameObject = delegate { };

    private void Start()
    {
        CreateOnStart();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GetGameObject();
        }
    }

    public void CreateOnStart()
    {
        for (int i = 0; i < numberOfObjects; i++)
        {
            GameObject tempObj = Instantiate(prifab,prifab.transform.position,prifab.transform.rotation) as GameObject;
            gameObjectsPool.Add(tempObj);
            tempObj.transform.SetParent(transform);
            tempObj.SetActive(false);
        }
    }

    public GameObject GetObject(int index)
    {
        if (index == gameObjectsPool.Count) { CreateOnStart();}
        gameObjectsPool[index].SetActive(true);
        GameObject tempObj = gameObjectsPool[index];
        return tempObj;
    }
}
