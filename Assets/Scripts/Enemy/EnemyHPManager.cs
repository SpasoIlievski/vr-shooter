﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHPManager : MonoBehaviour,ITakeRaycast
{
    private EnemyHordeManager hordeManager;
    [SerializeField] private FloatValue enemyHP,playerScore;
    [SerializeField] private float killScore;
    private float currentHealth;

    void Start()
    {
        hordeManager = FindObjectOfType<EnemyHordeManager>();
        currentHealth = enemyHP.Value;    
    }

    public void TakeDamege(float damage)
    {
        currentHealth -= damage;
        CheckHP();
    }

    private void CheckHP()
    {
        if (currentHealth <= 0)
        {
            currentHealth = enemyHP.Value;
            playerScore.Value += killScore;
            hordeManager.RemoveEnemyFromList(this.transform);
        }
    }
}
