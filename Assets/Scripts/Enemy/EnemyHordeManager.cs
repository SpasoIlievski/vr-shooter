﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHordeManager : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private GameObjectPool objectPool;
    [SerializeField] private int numberOfEnemies;
    private List<Transform> movingEnemies = new List<Transform>();

    private void Start()
    {
        objectPool.GetGameObject += GetEnemiesFromPool;
    }

    private void FixedUpdate()
    {
        if (movingEnemies.Count > 0 && player!=null)
        {
            foreach (var item in movingEnemies)
            {
                item.transform.RotateAround(player.transform.position, Vector3.up, 20 * Time.fixedDeltaTime);
            }
        }
    }

   public void GetEnemiesFromPool()
   {
        for (int i = 0; i < numberOfEnemies; i++)
        {
            movingEnemies.Add(objectPool.GetObject(i).transform);
            objectPool.GetObject(i).transform.position = new Vector3(Random.Range(-10f,11f),1,Random.Range(10f,20f));
            objectPool.GetObject(i).transform.SetParent(transform);
            objectPool.GetObject(i).transform.LookAt(player.transform);
        }
        numberOfEnemies += 1;
   }

    public void RemoveEnemyFromList(Transform enemyToRemove)
    {
        enemyToRemove.gameObject.SetActive(false);
        enemyToRemove.SetParent(objectPool.transform);
        movingEnemies.Remove(enemyToRemove);
        CheckActiveEnemies();
    }

    private void CheckActiveEnemies()
    {
        if(movingEnemies.Count==0)
        {
            GetEnemiesFromPool();
        }
    }
}
