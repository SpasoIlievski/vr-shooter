﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour,IShoot
{
    [SerializeField] private GameObject Muzzle;
    [SerializeField] private FloatValue enemyDamage;
    private Shoot enemyShoot;

    private void Start()
    {
        enemyShoot = new Shoot(this);
        StartCoroutine(ShootThePlayer());
    }

    public Transform GetMuzzleCoordinates()
    {
        return Muzzle.transform;
    }

    private IEnumerator ShootThePlayer()
    {
        yield return new WaitForSeconds(Random.Range(2f,5f));
        {
            enemyShoot.ShootProjectile();
            StartCoroutine(ShootThePlayer());
        }
    }

    public float GetDamage()
    {
        return enemyDamage.Value;
    }
}
