﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private TextMeshPro playerScoreText,playerHitPoints;
    [SerializeField] private FloatValue playerScore, playerHP;
    [SerializeField] private PlayerManager playerManager;
    [SerializeField] private GameObject tutorialPanel,gameOverPanel;


    private void Start()
    {
        playerManager.OnDeath.Add(OnPlayerDeath);
    }

    private void Update()
    {
        playerScoreText.text = playerScore.Value.ToString();
        playerHitPoints.text = playerHP.Value.ToString();
    }

    private void OnPlayerDeath()
    {
        gameOverPanel.SetActive(true);
    }

    public void StartPlaySession()
    {
        tutorialPanel.SetActive(false);
    }
}
