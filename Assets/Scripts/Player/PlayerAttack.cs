﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour, IShoot
{
    [SerializeField] private GameObject Muzzle;
    [SerializeField] private FloatValue playerDamage;


    public float GetDamage()
    {
        return playerDamage.Value;
    }

    public Transform GetMuzzleCoordinates()
    {
        return Muzzle.transform;
    }
}
