﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHPManager : MonoBehaviour,ITakeRaycast
{
    [SerializeField] private FloatValue startingPlayerHp,currentPlayerHP;
    [SerializeField] private PlayerManager playerManager;

    void Start()
    {
        currentPlayerHP.Value = startingPlayerHp.Value;
    }
   
    public void TakeDamege(float damage)
    {
        currentPlayerHP.Value -= damage;
        if (currentPlayerHP.Value <= 0)
        {
            playerManager.PlayerDeathActions();
        }
    }


   
    
}
