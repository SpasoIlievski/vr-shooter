﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{

    [SerializeField] private Camera playerCamera;
    [SerializeField] private GameObject gun;
    [SerializeField] private PlayerHPManager hpManager;
    public List<Action> OnDeath = new List<Action>();

    void Start ()
    {
       OnDeath.Add(OnPlayerDeath);
	}
	

    private void OnPlayerDeath()
    {
        Destroy(gun);
        playerCamera.transform.SetParent(null);
        playerCamera.transform.position = new Vector3(0,10,0);
    }

    public void PlayerDeathActions()
    {
        foreach (var item in OnDeath)
        {
            item();
        }
        Destroy(this.gameObject,1f);  
    }
}
