﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerElement : MonoBehaviour, ITakeRaycast
{
   [SerializeField] private UIManager uiManager;
   [SerializeField] private EnemyHordeManager hordeManager;


    public void TakeDamege(float damage)
    {
        uiManager.StartPlaySession();
        hordeManager.GetEnemiesFromPool();
        Destroy(gameObject);
    }
}
