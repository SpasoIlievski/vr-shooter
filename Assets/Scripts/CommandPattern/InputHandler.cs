﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    [SerializeField] private PlayerAttack playerAttack;
    private CommandHandler commandHandler = new CommandHandler();
	
	// Update is called once per frame
	void Update ()
    {
        for (int i = 0; i < Input.touchCount; ++i)
        {
            if (Input.GetTouch(i).phase.Equals(TouchPhase.Began))
            {
                Shoot playerShoot = new Shoot(playerAttack);
                commandHandler.ShootCommand(playerShoot);
            }
        }
        if (Input.GetMouseButton(0))
        {
            Shoot playerShoot = new Shoot(playerAttack);
            commandHandler.ShootCommand(playerShoot);
        }
	}
}
