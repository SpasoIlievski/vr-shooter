﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandHandler : ICommand
{
    public void ShootCommand(Shoot shoot)
    {
        shoot.ShootProjectile();
    }
}
