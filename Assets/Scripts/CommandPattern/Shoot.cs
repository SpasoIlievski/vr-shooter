﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot 
{
    private IShoot shotVariables;

    public Shoot(IShoot shoot)
    {
        shotVariables = shoot;
    }

    public void ShootProjectile()
    {
        Vector3 fwd = shotVariables.GetMuzzleCoordinates().transform.TransformDirection(Vector3.forward);
        Debug.DrawRay(shotVariables.GetMuzzleCoordinates().transform.position, fwd * 500, Color.red);
        RaycastHit objectHit;
        if (Physics.Raycast(shotVariables.GetMuzzleCoordinates().position, shotVariables.GetMuzzleCoordinates().forward, out objectHit, 500))
        {
            Debug.Log(objectHit.collider.gameObject.name);
            try
            {
                ITakeRaycast health = objectHit.collider.gameObject.GetComponent(typeof(ITakeRaycast)) as ITakeRaycast;
                health.TakeDamege(shotVariables.GetDamage());
              
            }
            catch 
            {
                
            } 
        }
    }
}

